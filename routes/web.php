<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login from CAS Client
Route::get('/cas/login',function(){
    return cas()->authenticate();
});


Route::get('/', function () {
    echo '<a href="/cas/login">Log Masuk</a>';
    // return view('welcome');
});
