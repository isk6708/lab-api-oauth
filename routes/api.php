<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Login from CAS Client
Route::get('/cas/login',function(){
    return cas()->authenticate();
});

Route::get('/welcome',function(){
    return ['status'=>true,'message'=>'Welcome Raj'];
});


Route::group(['middleware' => 'auth:sanctum'], function() {
    Route::post('/simpan-produk',[ProductController::class,'store']);
    Route::post('/kemaskini-produk/{id}',[ProductController::class,'update']);
    Route::delete('/hapus-produk/{id}',[ProductController::class,'destroy']);
    Route::post('/logout',[AuthController::class,'logout']);
});

Route::get('/senarai-produk',[ProductController::class,'index']);

Route::get('/dummy-pass',[AuthController::class,'hashPass']);
Route::post('/login',[AuthController::class,'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

