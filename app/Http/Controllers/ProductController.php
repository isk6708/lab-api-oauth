<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $q = $req->q;
        // $product_cat = $req->product_cat;
        return Product::where(function($query) use ($q){
            if (!empty($q)){
                $query->OrWhere('product_name','like','%'.$q.'%');
                $query->OrWhere('product_cat',$q);
            }
            
        })
        // ->where(function($q) use ($product_cat){
        //     if (!empty($product_cat))
        //     $q->where('product_cat',$product_cat);
        // })
        ->get();
    }

  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $val = validate([
        //     'product_name'=>'required'
        // ]);
        // if ($val){
            $mprod = new Product();
            $mprod->product_name = $request->product_name;
            $mprod->product_cat = $request->product_cat;
            $mprod->save();
        
            return $mprod;
        // }else{
        //     return ['error'=>true,'message'=>'Sila isikan maklumat diperlukan']
        // }
        
    }

   


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mprod = Product::find($id);
        $mprod->product_name = $request->product_name;
        $mprod->product_cat = $request->product_cat;
        $mprod->save();
        
        return $mprod;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mprod = Product::find($id);
        if ($mprod){
            Product::find($id)->delete();
            return ['status'=>'success','message'=>'Record sucessfully deleted'];
        }else{
            return ['status'=>'failed','message'=>'Record not exist'];
        }
    }
}
